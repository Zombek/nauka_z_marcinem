﻿using nauka.AbstractClasses;
using nauka.Interfaces;
using System;

namespace nauka.Vehicles
{
    class FlyingVehicle : BaseVehicle
    {
        public FlyingVehicle(ILogger logger) : base(logger) 
        {
        }

        public override void Delivery() 
        {
            _baseLogger.LogInfo("Dowóz lotniczy");
        }
    }
}
