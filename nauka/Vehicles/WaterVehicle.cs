﻿using nauka.AbstractClasses;
using nauka.Interfaces;
using System;

namespace nauka.Vehicles
{
    class WaterVehicle : BaseVehicle
    {
        public WaterVehicle(ILogger logger) : base(logger)
        { 
        }

        public override void Delivery()
        {
            _baseLogger.LogInfo("Dowóz wodny");
        }
    }
}
