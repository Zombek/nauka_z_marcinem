﻿using nauka.AbstractClasses;
using nauka.Interfaces;
using System;

namespace nauka.Vehicles
{
    class CarVehicle : BaseVehicle
    {
        public CarVehicle(ILogger logger) : base(logger)
        {
        }

        public override void Delivery()
        {
            _baseLogger.LogInfo("Dowóz autem");
        }
    }
}
