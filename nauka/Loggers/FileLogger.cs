﻿using nauka.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace nauka.Loggers
{
    class FileLogger : ILogger
    {
        private string _fullPath = "log.txt";
        public void LogInfo(string info)
        {
            bool appendLine = true; //dopisuj linijki
            using(StreamWriter writer = new StreamWriter(_fullPath, appendLine))
            {
                writer.WriteLine(info);
            }
        }
    }
}
