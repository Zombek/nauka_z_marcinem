﻿using nauka.Interfaces;
using System;

namespace nauka.Loggers
{
    class ConsoleLogger : ILogger
    {
        public void LogInfo(string info)
        {
            Console.WriteLine(info);
        }
    }
}
