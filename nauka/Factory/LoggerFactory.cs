﻿using nauka.Interfaces;
using nauka.Loggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nauka.Factory
{
    class LoggerFactory
    {
        public ILogger getLog(string kindOfLog)
        {
            switch (kindOfLog)
            {
                case "konsola":
                    return new ConsoleLogger();
                case "plik":
                    return new FileLogger();
                default:
                    return null;
            }
        }
    }
}
