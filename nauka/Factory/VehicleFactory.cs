﻿using nauka.AbstractClasses;
using nauka.Interfaces;
using nauka.Vehicles;
using System;

namespace nauka.Factory
{
    public class VehicleFactory
    {
        public BaseVehicle GetVehicle(string vehicleType, ILogger logger)
        {
            switch (vehicleType)
            {
                case "auto":
                    return new CarVehicle(logger);
                case "lot":
                    return new FlyingVehicle(logger);
                case "woda":
                    return new WaterVehicle(logger);
                default:
                    return null;
            }
        }
    }
}
