﻿using nauka.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nauka.AbstractClasses
{
    public abstract class BaseVehicle
    {
        protected ILogger _baseLogger;
        public BaseVehicle(ILogger logger)
        {
            _baseLogger = logger;
        }

        public abstract void Delivery();


    }
}
