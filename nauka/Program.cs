﻿using nauka.AbstractClasses;
using nauka.Factory;
using nauka.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;

namespace nauka
{
    class Program
    {
        static void Main(string[] args)
        {
            string typeOfLogs = "plik";
            LoggerFactory loggerFactory = new LoggerFactory();
            ILogger logger = loggerFactory.getLog(typeOfLogs);

            logger.LogInfo("Jak dowieźć: auto, lot, woda");
            string vehicleType = Console.ReadLine();
            VehicleFactory factory = new VehicleFactory();
            BaseVehicle vehicle = factory.GetVehicle(vehicleType, logger);
            
            try
            {
                vehicle.Delivery();
            }
            catch (NullReferenceException ex)
            {
                logger.LogInfo("Nie ma takiego dowozu");
            }
            catch (Exception e)
            {
                logger.LogInfo("Nieobsłużony błąd");
            }
        }
    }
}
